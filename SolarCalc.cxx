#include <utility>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <regex>
#include "SolarCalc.h"

// Check if the provided argument is a number but not e.g. some environmental variable
bool isNumber(std::string const& arg)
{
  static std::regex rx(R"([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)");
  return std::regex_match(arg, rx);
}

int main(int argc, char* argv[])
{
	// To remove program name from the list of arguments
	argc-=(argc>0); argv+=(argc>0);

	SolarCalculator sc;

	if (argc < 2) {
		printf("\033[1;31m ERROR: Missing arguments \n USAGE: ./SolarCalc [Power consumption in [V]] [Average number of sunshine hours per day] [Battery voltage in [V] is optional] [Battery capacity in [A*h] is optional]\n");
		return -1;
	}

	// Check if input args exist and are numbers but not some non-sense
	if ( isNumber(argv[0]) ) sc.m_PowerCons = atof(argv[0]);

	if ( isNumber(argv[1]) ) sc.m_Sundays = atof(argv[1]);

	if ( argv[2] && isNumber(argv[2]) ) {
		sc.m_V = atof(argv[2]);
	}

	if ( argv[3] && isNumber(argv[3]) ) {
		sc.m_C = atof(argv[3]);
	}

	// Check input arguments
	printf(" Power consumption: %.1f W\n Average number of sunshine hours: %.1f\n Solar panel capacity: %.1f W \n Battery voltage: %.1f V\n Battery capacity: %.1f A*h\n", 
			sc.m_PowerCons, sc.m_Sundays, sc.m_Wsolar, sc.m_V, sc.m_C);

	// Since one cannot install a fraction of a panel or battery, the result is rounded upwards
	int numpanels    = ceil(sc.nSolarpanels(sc.m_PowerCons, sc.m_Sundays, sc.m_Wsolar));
	int numBatteries = ceil(sc.nBatteries(sc.m_PowerCons, sc.m_Sundays, sc.m_V, sc.m_C));
	double panelArea = numpanels * sc.m_panelSize;

	printf("\033[1;36mCongratulations, you require at most %d solar panel(s), %.2f m^2 of roof area and %d battery(ies) to cover your energy needs. \n", 
		numpanels, panelArea, numBatteries);

	return 0;
}




