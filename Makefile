CC=g++

CFLAGS=-c -Wall -std=c++0x

CNAME=SolarCalc

all: $(CNAME)

$(CNAME): $(CNAME).o
	$(CC) $(CNAME).o -o $(CNAME)

$(CNAME).o: $(CNAME).cxx
	$(CC) $(CFLAGS) $(CNAME).cxx

clean:
	rm -rf *.o $(CNAME)