__doc__ = """
exec_calc.py
Simple python script to parse options and execute main C++ part
"""

from optparse import OptionParser
from subprocess import Popen, PIPE, STDOUT
from glob import glob
import re, sys, os, string

def exec_shell(cmd):
    """ exec_shell(cmd) => status, stdout, stderr: execute @cmd on a shell"""
    p = Popen(cmd, shell=True, stdout=PIPE, stderr=STDOUT)
    out, err = p.communicate()
    retval = p.returncode
    return retval, out, err

def main():

	# location of this script
    exec_path = os.path.abspath(os.path.dirname(sys.argv[0]))

    # Configure parser
    parser = OptionParser(usage="usage: %prog --powerCons=200 --powerCons=6 --Vbattery=24 --Cbattery=50")
    parser.add_option("--powerCons", dest="powerCons", type=float, help="user power consumption per day", default=None)
    parser.add_option("--sundays", dest="sundays", type=float, help="average number of sunshine days in user's location", default=None)
    parser.add_option("--Vbattery", dest="Vbattery", type=float, help="battery voltage in [V]", default=12.)
    parser.add_option("--Cbattery", dest="Cbattery", type=float, help="battery capacity in [A*h]", default=150.)

    # Parse input arguments
    options, args = parser.parse_args()

    exec_name = "SolarCalc"
    # Args consistency checks
    if not options.powerCons:
    	print "\033[1;31m ERROR: Missing required argument '--powerCons'"
    	parser.print_help()
    	sys.exit(1)

    if not options.sundays:
    	print "\033[1;31m ERROR: Missing required argument '--sundays'"
    	parser.print_help()
    	sys.exit(1)

    retval, out, err = exec_shell("make clean; make ; ./%s %f %f %f %f" % (exec_name, options.powerCons, options.sundays, options.Vbattery, options.Cbattery))

    if not err:
    	print "No errors so far! \n"

    print out

if __name__ == "__main__":
    main()

